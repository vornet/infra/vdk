%global _python_dist_allow_version_zero 1
%global srcname __VDK_PYLIB_MODNAME__
%global vdk_pylib_req vdk-pylib >= __VDEP_VDK_PYLIB_GE__, vdk-pylib < __VDEP_VDK_PYLIB_LT__


Name:           __MKIT_PROJ_PKGNAME__
Version:        __MKIT_PROJ_VERSION__
Release:        1%{?dist}
Summary:        __VDK_PYLIB_SUMMARY__
URL:            __MKIT_PROJ_VCS_BROWSER__
License:        __MKIT_PROJ_LICENSE__
Source0:        %{name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  %{vdk_pylib_req}

%description
__VDK_PYLIB_DESCRIPTION__

Built with vdk-pylib-__VDK_PYLIB_VDK_VERSION__


%package -n python3-%{srcname}
Summary:        %{summary}
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  __VDK_FEDORA_BUILDREQUIRES__
BuildRequires:  __VDK_FEDORA_REQUIRES__
Requires:       __VDK_FEDORA_REQUIRES__
%description -n python3-%{srcname}
__VDK_PYLIB_DESCRIPTION__


%prep
%setup -q


%build
make %{?_smp_mflags} PREFIX=/usr pystuff
%py3_build

%install
%py3_install

%check
export VDK_TEST__DEBUG=true
export VDK_TEST__PYLIB_MODNAME=%{srcname}
%{_vdk_utildir}/vdk_test mypy \
  && %{_vdk_utildir}/vdk_test doctest \
  && %{_vdk_utildir}/vdk_test unittest \
  && %{_vdk_utildir}/vdk_test pytest

%files -n python3-%{srcname}
%{python3_sitelib}/%{srcname}-*.egg-info
%{python3_sitelib}/%{srcname}/


%changelog

# vim: syntax=spec:
# specfile built with MKit __MKIT_MKIT_VERSION__ and vdk-pylib-__VDK_PYLIB_VDK_VERSION__
