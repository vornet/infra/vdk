%global vdk_zigbin_req vdk-zigbin >= __VDEP_VDK_ZIGBIN_GE__, vdk-zigbin < __VDEP_VDK_ZIGBIN_LT__
%global zigdev_req zigdev >= __VDEP_THEIR_ZIGDEV_GE__, zigdev < __VDEP_THEIR_ZIGDEV_LT__
%global debug_package %{nil}


Name:           __MKIT_PROJ_PKGNAME__
Version:        __MKIT_PROJ_VERSION__
Release:        1%{?dist}
Summary:        __VDK_ZIGBIN_SUMMARY__
URL:            __MKIT_PROJ_VCS_BROWSER__
License:        __MKIT_PROJ_LICENSE__
Source0:        %{name}-%{version}.tar.gz
BuildRequires:  %{vdk_zigbin_req}
BuildRequires:  %{zigdev_req}
BuildRequires:  __VDK_FEDORA_BUILDREQUIRES__
BuildRequires:  __VDK_FEDORA_REQUIRES__
Requires:       __VDK_FEDORA_REQUIRES__

%description
__VDK_ZIGBIN_DESCRIPTION__

Built with vdk-zigbin-__VDK_ZIGBIN_VDK_VERSION__


%prep
%setup -q


%build
zig build --verbose --summary all --verbose-cc install


%install
make PREFIX=/usr DESTDIR="$RPM_BUILD_ROOT" install
touch pkg.files
find "$RPM_BUILD_ROOT" -type d -mindepth 1 \
  | sed "s:$RPM_BUILD_ROOT/:%dir /:" \
  | sort >> pkg.files
find "$RPM_BUILD_ROOT" -type f \
  | sed "s:$RPM_BUILD_ROOT/:/:" \
  | sort >> pkg.files


%check
true zig build --verbose --summary all --verbose-cc test


%files -f pkg.files


%changelog

# vim: syntax=spec:
# specfile built with MKit __MKIT_MKIT_VERSION__ and vdk-zigbin-__VDK_ZIGBIN_VDK_VERSION__
