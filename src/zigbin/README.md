VDK-ZIGBIN
==========

vdk-zigbin is a MKit-based toolkit providing templates, plugins and build
targets specific for simple Zig projects.
