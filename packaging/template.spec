%global zigdev_req zigdev >= __VDEP_OUR_ZIGDEV_GE__, zigdev < __VDEP_OUR_ZIGDEV_LT__

Name:       __MKIT_PROJ_PKGNAME__
Version:    __MKIT_PROJ_VERSION__
Release:    1%{?dist}
Summary:    __PROJECT_SUMMARY__
URL:        __MKIT_PROJ_VCS_BROWSER__
License:    LGPLv2
Source0:    %{name}-%{version}.tar.gz
BuildArch:  noarch
BuildRequires: /usr/bin/make

%description
__PROJECT_SUMMARY__

%package common
Requires: git
Requires: make
Summary: Vornet development kit, shared files
%description common

__PROJECT_SUMMARY__

This sub-package contains common utilities and scripts shared amongst
different kits.

%package pylib
Requires: git
Requires: make
Requires: python3-build
Requires: python3-mypy
Requires: python3-pytest
Requires: vdk-common == %{version}
Summary: Vornet development kit for Python libraries
%description pylib

__PROJECT_SUMMARY__

This sub-package contains templates and scripts specific for Python library
development kit.

%package zigbin
Requires: git
Requires: make
Requires: %{zigdev_req}
Requires: vdk-common == %{version}
Summary: Vornet development kit for simple Zig projects
%description zigbin

__PROJECT_SUMMARY__

This sub-package contains templates and scripts specific for Zig
development kit.

%prep
%setup -q

%build
make %{?_smp_mflags} PREFIX=/usr

%install
%make_install PREFIX=/usr

%files common
%dir %{_datadir}/%{name}/utils/mkit
%dir %{_datadir}/%{name}/utils/mkit/include
%{_datadir}/%{name}/utils/mkit-plugins/vdk_test
%{_datadir}/%{name}/utils/mkit-plugins/vdk_test_cycle
%{_datadir}/%{name}/utils/mkit-plugins/vdk_test_pdb
%{_datadir}/%{name}/utils/mkit-plugins/vdk_test_shell
%{_datadir}/%{name}/utils/mkit-plugins/vdk_test_venv
%{_datadir}/%{name}/utils/mkit/include/build.sh
%{_datadir}/%{name}/utils/mkit/include/deploy.sh
%{_datadir}/%{name}/utils/mkit/include/facts.sh
%{_datadir}/%{name}/utils/mkit/include/ini.sh
%{_datadir}/%{name}/utils/mkit/include/mkit.sh
%{_datadir}/%{name}/utils/mkit/include/plugin.sh
%{_datadir}/%{name}/utils/mkit/include/release.sh
%{_datadir}/%{name}/utils/mkit/include/target.sh
%{_datadir}/%{name}/utils/mkit/include/util.sh
%{_datadir}/%{name}/utils/mkit/include/vars.sh
%{_datadir}/%{name}/utils/mkit/mkit
%{_datadir}/%{name}/utils/mkit/mkit.mk
%{_datadir}/%{name}/utils/mkit/plugins/debstuff
%{_datadir}/%{name}/utils/mkit/plugins/pystuff
%{_datadir}/%{name}/utils/mkit/plugins/rpmstuff
%{_datadir}/%{name}/utils/mkit/stub
%{_datadir}/%{name}/utils/vdk_test
%{_prefix}/lib/rpm/macros.d/macros.vdk

%files pylib
%dir %{_datadir}/%{name}/pylib/skel
%dir %{_datadir}/%{name}/pylib/skel/packaging
%dir %{_datadir}/%{name}/pylib/skel/packaging/debian
%dir %{_datadir}/%{name}/pylib/skel/packaging/debian/source
%doc %{_docdir}/%{name}/vdk-pylib.md
%{_datadir}/%{name}/pylib/Makefile
%{_datadir}/%{name}/pylib/skel/mkit.ini
%{_datadir}/%{name}/pylib/skel/packaging/debian/changelog
%{_datadir}/%{name}/pylib/skel/packaging/debian/control
%{_datadir}/%{name}/pylib/skel/packaging/debian/copyright
%{_datadir}/%{name}/pylib/skel/packaging/debian/install
%{_datadir}/%{name}/pylib/skel/packaging/debian/rules
%{_datadir}/%{name}/pylib/skel/packaging/debian/source/format
%{_datadir}/%{name}/pylib/skel/packaging/setup.py
%{_datadir}/%{name}/pylib/skel/packaging/template.spec

%files zigbin
%dir %{_datadir}/%{name}/zigbin/skel
%dir %{_datadir}/%{name}/zigbin/skel/packaging
%dir %{_datadir}/%{name}/zigbin/skel/packaging/debian
%dir %{_datadir}/%{name}/zigbin/skel/packaging/debian/source
%doc %{_docdir}/%{name}/vdk-zigbin.md
%{_datadir}/%{name}/zigbin/Makefile
%{_datadir}/%{name}/zigbin/skel/mkit.ini
%{_datadir}/%{name}/zigbin/skel/packaging/debian/changelog
%{_datadir}/%{name}/zigbin/skel/packaging/debian/control
%{_datadir}/%{name}/zigbin/skel/packaging/debian/copyright
%{_datadir}/%{name}/zigbin/skel/packaging/debian/install
%{_datadir}/%{name}/zigbin/skel/packaging/debian/rules
%{_datadir}/%{name}/zigbin/skel/packaging/debian/source/format
%{_datadir}/%{name}/zigbin/skel/packaging/template.spec

%changelog

# specfile built with MKit __MKIT_MKIT_VERSION__
